package by.shulga.type;


public enum SeedType {
    SMALL, MEDIUM, LARGE
}