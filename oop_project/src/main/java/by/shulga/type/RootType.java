package by.shulga.type;

public enum RootType {
    RHIZOME, BULB, TUBER;     //корневище, луковица, клубень
}