package by.shulga.type;


public enum ColorType {
    RED, YELLOW, WHITE, BLUE, GREEN, PURPLE;
}
