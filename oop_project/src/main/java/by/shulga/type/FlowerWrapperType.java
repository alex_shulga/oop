package by.shulga.type;

public enum FlowerWrapperType {
    ORGANZA, NET, FELT, PAPER, RIBBON
}
