package by.shulga.action;

import by.shulga.entity.AFlower;
import by.shulga.entity.Bouquet;
import java.util.ArrayList;
import java.util.List;

public class FindFlowers {
    public static List<AFlower> findInPriceRange(Bouquet flowers, int minCost, int maxCost) {
        List<AFlower> tempList = new ArrayList<>();
        for (AFlower temp : flowers.getBouquet()) {
            if (temp.getCost() > minCost && temp.getCost() < maxCost)
                tempList.add(temp);
        }
        return tempList;
    }
}
