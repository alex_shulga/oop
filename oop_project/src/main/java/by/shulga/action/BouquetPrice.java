package by.shulga.action;

import by.shulga.entity.AFlower;
import by.shulga.entity.Bouquet;

import java.util.ArrayList;

public class BouquetPrice {

    public static int summaryPrice(Bouquet flowers) {
        int price = 0;
    for (AFlower flag : flowers.getBouquet()) {
            price += flag.getCost();
        }
        return price;
    }
}
